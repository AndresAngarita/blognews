<?php
    session_start();
    if(!isset($_SESSION['user'])){
        header("location: login.php");
    }
    require(dirname(__FILE__,$levels=3)."/control/comments/CommentDao.php");
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="../imgs/iconTitle.jpg"/>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <title>Comentarios</title>
</head>
<body>
    <header class="bg-dark text-center">
        <div class="text-white h2 align-content-between">
        <p>Noticiero</p> 
        
        <button class="btn btn-md text-white" style="background-color:transparent;">
            <i class="fa fa-user text-white" ></i> Bienvenido <?php echo $_SESSION["user"];?>
        </button>    
        <a name="salir" id="salir" class="btn btn-danger" href="../logout.php" role="button" title="salir"><i class="fa fa-power-off"></i></a>
        
    </div>
    </header>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item" aria-current="page"><a href="../home/0">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">comentarios</li>
        </ol>
    </nav>
    
    <div class = "container">
        <div class ="row">
            <div class="col-1"></div>

            <div class="col-11 table-responsive">
                <table class="table table-sm table-dark ">
                    <thead class="thead-inverse text-sm-center">
                        <tr>
                            <th>nombre</th>
                            <th>correo</th>
                            <th>comentario</th>
                            <th>fecha del comentario</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $commentDao = new CommentDao();
                    $id = $_GET['nt'];
                    foreach ($commentDao->selectByIdNoti($id) as $registro):?>
                        <tr>    
                        <?php foreach ($registro as $key => $dato):?>
                            <td scope="row"><?php echo $dato ?></td>
                        <?php endforeach?>
                        </tr>
                    <?php endforeach?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <br>
            </div>
        </div>
    </div>

    
<br>
    <footer class="page-footer font-small blue bg-dark fixed-bottom" >
        <!-- Copyright -->
        <div class="footer-copyright text-center text-white py-3">© 2019 Copyright:
        <a href="#"> RewDev.com</a>
        </div>
        <!-- Copyright -->

    </footer>

    <script src="js/bootstrap.min.js"></script>
</body>
</html>