<?php
session_start();
if(!isset($_SESSION['user'])){
    header("location: login.php");
}
require(dirname(__FILE__,$levels=3)."/control/categories/CategoryDao.php");
$categoryDao = new CategoryDao();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="../imgs/iconTitle.jpg"/>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <title>Add Noticia</title>
</head>
<body>
    <header class="bg-dark text-center">
        <div class="text-white h2">
            Noticiero
        </div>
    </header>
    
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item" aria-current="page"><a href="../home/0">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Agregar Noticia</li>
        </ol>
    </nav>

    <div class = "container">
        <div class ="row">
            <div class="col-3">
                
            </div>
            <div class="col-sm align-content-center">
                <p class="h3">Adicionar Noticia</p>
                <form action="../../control/news/New.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="opcion" id="opcion" class="form-control" value="1">
                    
                    <div class="form-group">
                      <label for="titulo">titulo</label>
                      <input type="text" class="form-control" name="titulo" id="titulo" placeholder="titulo">
                    </div>
                    <div class="form-group">
                      <label for="detalle">detalle</label>
                      <textarea class="form-control" name="detalle" id="detalle" rows="4"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="imagen">imagen</label>
                      <input type="file" class="form-control" name="imagen" id="imagen" placeholder="imagen">
                    </div>
                    <div class="form-group">
                        <label for="categoria">categoria</label>
                        <select class="custom-select" name="categoria" id="categoria">
                            <option selected>Select one</option>
                            <?php foreach ($categoryDao->selectAll() as $categoria):?>
                                <option value="<?php echo $categoria[0]?>"><?php echo $categoria[1]?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Subir</button>
                </form>
            </div>

            <div class="col-3">
                
            </div>

        </div>
    </div>

<br>
    <footer class="page-footer font-small blue bg-dark w-100">
        <!-- Copyright -->
        <div class="footer-copyright text-center text-white py-3">© 2019 Copyright:
            <a href="#"> RewDev.com</a>
        </div>
        <!-- Copyright -->

    </footer>
    <script src="../js/bootstrap.min.js"></script>
</body>
</html>