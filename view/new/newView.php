<?php
session_start();
if(!isset($_SESSION['user'])){
    header("location: login.php");
}
require(dirname(__FILE__,$levels=3)."/control/news/NewDao.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="../imgs/iconTitle.jpg"/>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <title>Noticia</title>
</head>
<body>
    <header class="bg-dark text-center">
        <div class="text-white h2 align-content-between">
        <p>Noticiero</p> 
        
        <button class="btn btn-md text-white" style="background-color:transparent;">
            <i class="fa fa-user text-white" ></i> Bienvenido <?php echo $_SESSION["user"];?>
        </button>    
        <a name="salir" id="salir" class="btn btn-danger" href="../logout.php" role="button" title="salir"><i class="fa fa-power-off"></i></a>
        
    </div>
    </header>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item" aria-current="page"><a href="../home/0">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Noticia</li>
        </ol>
    </nav>
        <?php $newDao = new NewDao();
          if ($noticia = $newDao->selectById($_GET['nt'])):
          ?>
            <div class="container">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-8">

                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $noticia['titulo']?></h5>
                                <h6 class="card-subtitle mb-2 text-muted"><?php echo date('F j, Y',strtotime($noticia['fecha_sis']))?></h6>
                                <img src="../<?php echo $noticia['img_path'].$noticia['img_name']?>" class="card-img-top img-fluid" alt="<?php echo $noticia['img_name']?>">
                                <p class="card-text"><?php echo $noticia['detalle']?></p>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Añadir Comentario</h5>
                                <form action="../../control/comments/Comment.php" method="post">
                                    <input type="hidden" name="opcion" id="opcion" class="form-control" value="1">
                                    <input type="hidden" name="noti" id="noti" class="form-control" value="<?php echo $noticia['id']?>">
                                    <input type="hidden" name="user" id="user" class="form-control" value="<?php echo $_SESSION['userId']?>">

                                    <div class="form-group">
                                      <label for="Comentario">Comentario</label>
                                      <textarea class="form-control" name="comentario" id="comentario" rows="3"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-2"></div>
                </div>        
            </div>
          <?php else:
            echo "Noticia no encontrada";
          endif;
          ?>    
    <footer class="page-footer font-small blue bg-dark">
        <!-- Copyright -->
        <div class="footer-copyright text-center text-white py-3">© 2019 Copyright:
        <a href="#"> RewDev.com</a>
        </div>
        <!-- Copyright -->

    </footer>

    <script src="js/bootstrap.min.js"></script>
</body>
</html>