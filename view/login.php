<?php
session_start();
if(isset($_SESSION['user'])){
    header("location: home/0");
}

require (dirname(__FILE__,$levels=2)."/control/connection/Connection.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="imgs/iconTitle.jpg"/>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <title>Login</title>
</head>
<body style="font-family: 'Roboto', sans-serif;">
    <header class="bg-dark text-center">
        <div class="text-white h2">
        Noticiero
        </div>
    </header>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Login</li>
        </ol>
    </nav>
    <div class="container">
        <div class="row" >
            <div class="col-4"></div>
            <div class="col-4 border" style="padding-top: 30px; margin-top: 40px;">
                <h3>Login</h3><br>
                <form action="../control/users/User.php" method="post">
                    <input type="hidden" id="opcion" name="opcion" class="form-control" value="3">
                    <div class="form-group">
                        <input type="text" name="usuario" id="usuario" class="form-control" placeholder="Your User *" value="" required>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Your Password *" value="" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-dark" value="Login">
                    </div>
                    <div class="form-group">
                        <div class="row justify-content-between no-gutters">
                            <a href="#" class="btnForgetPwd">Forget Password?</a>
                            <a href="user/registroUserView.php" class="Signin">Sign in</a>
                        </div>    
                    </div>
                </form>
            </div>
            <div class="col-4"></div>
        </div>
    </div>
    <br>
    <footer class="page-footer font-small blue bg-dark fixed-bottom">
        <!-- Copyright -->
        <div class="footer-copyright text-center text-white py-3">© 2019 Copyright:
        <a href="#"> RewDev.com</a>
        </div>
        <!-- Copyright -->

        </footer>
    
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
