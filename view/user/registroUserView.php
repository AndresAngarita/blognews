<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="imgs/iconTitle.jpg"/>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <title>Registro</title>
</head>
<body>
    <header class="bg-dark text-center">
        <div class="text-white h2">
            Noticiero
        </div>
    </header>
    
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Registro</li>
        </ol>
    </nav>

    <div class = "container">
        <div class ="row">
            <div class="col-3">
                
            </div>
            <div class="col-sm align-content-center">
                <p class="h3">Registro Usuario</p>
                <form action="../../control/users/User.php" method="post">
                    
                    <input type="hidden" name="opcion" id="opcion" class="form-control" value="1">
                    
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" name="nombre" id="nombre" placeholder="nombre" class="form-control" aria-describedby="nombre" required>
                    </div>
                    <div class="form-group">
                        <label for="correo">Correo</label>
                        <input type="text" name="correo" id="correo" placeholder="E-mail" class="form-control" aria-describedby="correo" required>
                    </div>
                    <div class="form-group">
                        <label for="usuario">Usuario</label>
                        <input type="text" name="usuario" id="usuario" placeholder="usuario" class="form-control" aria-describedby="Usuario" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" placeholder="password" class="form-control" aria-describedby="Password" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Registrar</button>
                </form>
            </div>

            <div class="col-3">
                
            </div>

        </div>
    </div>

<br>
    <footer class="page-footer font-small blue bg-dark w-100">
        <!-- Copyright -->
        <div class="footer-copyright text-center text-white py-3">© 2019 Copyright:
            <a href="#"> RewDev.com</a>
        </div>
        <!-- Copyright -->

    </footer>


    <script src="../js/bootstrap.min.js"></script>
</body>
</html>