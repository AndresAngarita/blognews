<?php
    session_start();
    if(!isset($_SESSION['user'])){
        header("location: ../login.php");
    }
    require(dirname(__FILE__,$levels=3)."/control/users/User.php");
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <title>Update</title>
</head>
<body>
    <header class="bg-dark text-center">
        <div class="text-white h2">
            Page
        </div>
        <ul class="nav navbar-dark bg-dark justify-content-center nav-pills h5">
            <li class="nav-item">
                <a href="../home/0" class="nav-link text-white">Home</a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link active">Empleados</a>
            </li>
            <li class="nav-item">
            <a href="#" class="nav-link text-white">Reportes</a>
            </li>
        </ul>
        <a name="salir" id="salir" class="btn btn-danger float-right" href="logout.php" role="button">Salir</a>
    </header>
    
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="userssView.php">Empleados</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar</li>
        </ol>
    </nav>

    <div class = "container">
        <div class ="row">
            <div class="col-3">
                
            </div>
            <div class="col-sm align-content-center">
                <p class="h3">Registro Usuario</p>
                <form action="../../control/users/Empleado.php" method="post">
                    
                    <input type="hidden" name="opcion" id="opcion" class="form-control" value="2">
                    <?php foreach ($users->selectById($_GET['k']) as $key=> $registro):?>
                        <?php if($key=="id"):?>
                            <input type="hidden" name="id" id="id" class="form-control" value=<?php echo $registro?>>
                        <?php else:?>        
                            <div class="form-group">
                                <label for="<?php echo $key?>"><?php echo $key?></label>
                                <input type="text" name="<?php echo $key?>" id="<?php echo $key?>" class="form-control" aria-describedby="Documento" value ="<?php echo $registro?>" required>
                            </div>
                        <?php endif?>    
                    <?php endforeach?>
                    <button type="submit" class="btn btn-primary">Modificar</button>
                </form>
            </div>

            <div class="col-3">
            </div>
        </div>
    </div>

<br>
    <footer class="page-footer font-small blue bg-dark w-100">
        <!-- Copyright -->
        <div class="footer-copyright text-center text-white py-3">© 2019 Copyright:
            <a href="#"> RewDev.com</a>
        </div>
        <!-- Copyright -->

    </footer>
    <script src="../js/bootstrap.min.js"></script>
</body>
</html>