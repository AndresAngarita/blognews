<?php
session_start();
if(!isset($_SESSION['user'])){
    header("location: login.php");
}
require(dirname(__FILE__,$levels=2)."/control/news/NewDao.php");
require(dirname(__FILE__,$levels=2)."/control/comments/CommentDao.php");
require(dirname(__FILE__,$levels=2)."/control/categories/CategoryDao.php");

$newDao = new NewDao();
$commentDao = new CommentDao();
$categoryDao = new CategoryDao();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="../imgs/iconTitle.jpg"/>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../lib/jquery-ui-1.12.1/jquery-ui.css">
    <script src="../../lib/jquery-ui-1.12.1/jquery-3.4.1.js"></script>
    <script src="../../lib/jquery-ui-1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script>
        $( function() {
            $( "#accordionCategorias" ).accordion({
            collapsible: true
            });
        } );
    </script>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <title>Blog</title>
</head>
<body style="font-family: 'Roboto', sans-serif;">
    <header class="bg-dark text-center">
        <div class="text-white h2 align-content-between">
        <p>Noticiero</p> 
        
        <button class="btn btn-md text-white" style="background-color:transparent;">
            <i class="fa fa-user text-white" ></i> Bienvenido <?php echo $_SESSION["user"];?>
        </button>    
        <a name="salir" id="salir" class="btn btn-danger" href="../logout.php" role="button" title="salir"><i class="fa fa-power-off"></i></a>
        
    </div>
    </header>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Home</li>
        </ol>
    </nav>
    <?php if($_SESSION['role']=="Admin"): ?>
        <a name="addNew" id="addNew" class="btn btn-outline-success float-sm-left" href="../new/registroNewView.php" role="button">Agregar Noticia</a><br>
    <?php endif;?> 
       
    <div class="container">
        <br>
        <div class="row">
            <div class=col-8>
                <!-- inician noticias -->
                <?php
                    $result = $newDao->selectAll();
                    $nRows = $newDao->selectCount();
                    $page = $_GET['page'];
                    $cantView = 1;
                    $limEnd = ($nRows>(($page+1)*$cantView)) ? (($page+1)*$cantView) : $nRows ;
                    
                    //for que imprime la noticia
                    for ($i=($page*$cantView); $i < $limEnd; $i++):?>
                    <div class="card w-100">
                        <div class="card-header text-center"><?php echo $result[$i][1]?></div>
                        <div class="row justify-content-between">
                            <div class="card-body ">
                                <h6 class="card-subtitle text-muted">noticia<?php echo $result[$i][0]?></h6>
                            </div>
                            <div class="card-body ">
                                <a name="pdfNews" id="pdfNews" class="btn btn-outline-danger float-sm-right" href="../../control/news/New.php?r=<?php echo $result[$i][0]?>" role="button">PDF</a>
                                <h6 class="card-subtitle text-muted"><?php echo $result[$i][3]?></h6>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title text-center">Noticia #<?php echo $result[$i][0]?></h5>
                            
                            <img src="../<?php echo $result[$i][5].$result[$i][4]?>" class="card-img-top img-fluid" alt="noti1">
                            <p class="card-text"><?php echo $result[$i][2]?></p>
                            <div class="row justify-content-between no-gutters">
                                <a href="../new/noticia_<?php echo $result[$i][0]?>" class="card-link">Ver mas</a>
                                
                                <?php $nComments=$commentDao->selectCant($result[$i][0]);?>
                                <a href="../comment/comentario_<?php echo $result[$i][0]?>" class="card-link">Comentarios:<?php echo $nComments['coment']?></a>
                            </div>
                        </div>
                    </div>
                    <br>                    
                <?php endfor; ?>
                <!-- fin noticias -->
                <?php
                //si hay varias noticias crea la paginacion
                if ($nRows>$cantView):?>
                <nav aria-label="Page navigation new">
                    <ul class="pagination justify-content-center">
                        <?php if ($page==0):?>
                            <li class="page-item disabled"><a class="page-link" href="#" aria-disabled="true">Previous</a></li>
                        <?php else:?>
                            <li class="page-item"><a class="page-link" href="<?php echo ($page-1);?>" aria-disabled="false">Previous</a></li>
                        <?php endif;?>
                    
                        <?php for ($i=1; $i <= ceil($nRows/$cantView); $i++):?>
                            <?php if ($i==$page+1):?>
                                <li class="page-item active"><a class="page-link" href="<?php echo ($i-1);?>"> <?php echo $i;?> </a></li>
                            <?php else:?>
                                <li class="page-item"><a class="page-link" href="<?php echo ($i-1);?>"> <?php echo $i;?> </a></li>
                            <?php endif;?>
                            
                        <?php endfor;?>

                        <?php if ($page==ceil($nRows/$cantView)-1):?>
                            <li class="page-item disabled"><a class="page-link" href="#" aria-disabled="true">Next</a></li>
                        <?php else:?>
                            <li class="page-item"><a class="page-link" href="<?php echo ($page+1);?>" aria-disabled="false">Next</a></li>
                        <?php endif;?>
                        
                    </ul>
                </nav>
                <?php endif;?>
              
            </div>
            <div class ="col-4">
                <h3>Categories</h3>
                <br>
                <div class="accordion" id="accordionCategorias">
                    <?php 
                        foreach ($categoryDao->selectAll() as $registro): 
                    ?>
                        <h4><?php echo $registro[1];?></h4>
                        <ul>
                            <?php 
                                $newsCat=$newDao->selectByIdCategory($registro[0]);
                                foreach ($newsCat as $newCate): 
                            ?>
                                <li><a href="../new/noticia_<?php echo $newCate[0]?>" class="btn-link">-<?php echo $newCate[1];?></a><br></li>
                            <?php endforeach;?>
                        </ul>        
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
    <br>
    <footer class="page-footer font-small blue bg-dark">
        <!-- Copyright -->
        <div class="footer-copyright text-center text-white py-3">© 2019 Copyright:
        <a href="#"> RewDev.com</a>
        </div>
        <!-- Copyright -->

        </footer>

    <script src="js/bootstrap.min.js"></script>
</body>
</html>
