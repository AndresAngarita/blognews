CREATE DATABASE IF NOT EXISTS `news` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;

USE `news`;

CREATE TABLE IF NOT EXISTS `news`.`categorias`(
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(100) NOT NULL,
    PRIMARY KEY(`id`)
);

CREATE TABLE IF NOT EXISTS `news`.`noticias`(
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `titulo` varchar(100) NOT NULL,
    `detalle` varchar(1000) NOT NULL,
    `fecha_sis` date NOT NULL,
    `img_name` varchar(80) NOT NULL,
    `img_path` varchar(250) NOT NULL,
    `id_categoria` int(11) NOT NULL,
    PRIMARY KEY(`id`),
    FOREIGN KEY (`id_categoria`) REFERENCES `news`.`categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `news`.`comentarios`(
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `comentario` text NOT NULL,
    `fecha_comment` date NOT NULL,
    `id_usuario` int(11) NOT NULL,
    `id_noticia` int(11) NOT NULL,
    PRIMARY KEY(`id`),
    FOREIGN KEY (`id_usuario`) REFERENCES `news`.`usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    FOREIGN KEY (`id_noticia`) REFERENCES `news`.`noticias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `news`.`usuario` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `nombre` VARCHAR(80) NOT NULL , 
    `correo` VARCHAR(150) NOT NULL , 
    `usuario` VARCHAR(50) NOT NULL , 
    `password` VARCHAR(150) NOT NULL , 
    `role` ENUM('User', 'Admin') NOT NULL,
    PRIMARY KEY (`id`)
); 

INSERT INTO usuario (id, nombre, correo, usuario, password, role) VALUES
(NULL, 'Rick', 'rick@gmail.com', 'Rick', 'f482d881473df5ad52a2ea3623e1fc8b', 'Admin');

INSERT INTO categorias (id,descripcion) VALUES 
(null,'Machine Learning'),
(null,'Artificial Inteligence'),
(null,'Deep Learning'),
(null,'Neural Networks'),
(null,'Time Series'),
(null,'Blockchain'),
(null,'3D Modeling'),
(null,'Animation'),
(null,'Sculpt'),
(null,'Videogames');

ALTER TABLE noticias AUTO_INCREMENT = 1;

INSERT INTO noticias (id,titulo,detalle,fecha_sis,img_name,img_path,id_categoria) VALUES 
(null,'machine learning desde un entorno rural','Inicios. Metaphase empezo en un garaje, siguiendo la tradicion de las grandes tecnologicas. Arranco como un pequeño estudio de videojuegos creada por tres estudiantes de la Universidad de Granada. Actividad. El software es para Metaphase como el trigo para el pan. Esta ingenieria diseña soluciones a la carta. Allianz Seguros yEstacionamientos y Servicios son sus grandes clientes. Negocio. En su base de Alburquerque trabajan 26 personas, una plantilla paritaria formada en gran parte por ingenieros. Su cifra de negocio ronda el millon de euros y duplica la de 2016.Tomado de:https://www.hoy.es/economia/tribunabbva/metaphase07-machine-learning-20191130201500-nt.html ','2019-11-30','metaphase.jpg','imgs/',1),
(null,'China, nueva potencia tecnologica','En la entrada de la Ciudad Prohibida, en Beijing, una larga fila de visitantes espera pacientemente ingresar a este sitio historico construido durante la Dinastia Ming hace mas de 500 años. El acceso para los locales es rapido: un policia escanea el rostro del visitante al tiempo que la persona paga desde su celular, no es necesario mostrar una identificacion o tener dinero en el bolsillo, todo se realiza de manera digital. Este es solo un ejemplo de lo que hoy sucede en China, pais que en los pasados 10 años ha invertido agresivamente en la creacion de "startups" y en la expansion de nuevas tecnologias como la Inteligencia artificial, la nube, la realidad aumentada, el "machine learning" y la analitica entre otras. ¿Cual ha sido el camino que ha seguido para lograr estos avances? Tomado de:https://www.el-mexicano.com.mx/cienciaytecnologia/china-nueva-potencia-tecnologica-/2040162','2019-11-29','china.jpg','imgs/',2),
(null,'Dia de la Ciberseguridad: el 67% de las compañias preve invertir en tecnologias antifraude','Las nuevas tecnologias y los sistemas de aprendizaje inteligentes pueden ser claves para reducir el numero de ataques informaticos. La cuestion de la ciberseguridad empieza a tenerse muy en cuenta. Se trata de combatir el fraude a escala industrial. En Europa, los ciberataques mas comunes a las organizaciones son el robo de datos, robo de identidad y cuentas corrientes, tal y como se desprende de del informe EMEA Fraud Report 2019 de Experian, compañia tecnologica especializada en servicios crediticios, analitica avanzada y data, realizado por la consultora Forrester. Tomado de: https://www.directivosyempresas.com/internet/tecnologia/dia-mundial-de-la-ciberseguridad-tecnologias-antifraude-experian/','2019-11-30','seguridad.png','imgs/',1),
(null,'noticia 4','lorem ipsum lorem ipsumlorem ipsum','2019-05-12','newsAi1.jpg','imgs/',3),
(null,'noticia 5','lorem ipsum lorem ipsumlorem ipsum','2019-05-12','newsAi1.jpg','imgs/',4);

INSERT INTO comentarios (id, comentario, fecha_comment, id_usuario, id_noticia) VALUES
(NULL, 'hola, con respecto a la noticia y viendo desde un punto de vista critico analizando profundamente el contenido pienso que de acuerdo a lo hablado anteriormente se puede concluir claramente y con argumentos irrefutables lo que a continuacion se menciona,no sin antes olvidar darle gracias al creador del sitio.', '2019-12-10', '1' ,'1'),
(NULL, 'hola, con respecto a la noticia y viendo desde un punto de vista critico analizando profundamente el contenido pienso que de acuerdo a lo hablado anteriormente se puede concluir claramente y con argumentos irrefutables lo que a continuacion se menciona,no sin antes olvidar darle gracias al creador del sitio.', '2019-12-10', '1' ,'1'),
(NULL, 'hola, con respecto a la noticia y viendo desde un punto de vista critico analizando profundamente el contenido pienso que de acuerdo a lo hablado anteriormente se puede concluir claramente y con argumentos irrefutables lo que a continuacion se menciona,no sin antes olvidar darle gracias al creador del sitio.', '2019-12-10', '1' ,'2'),
(NULL, 'hola, con respecto a la noticia y viendo desde un punto de vista critico analizando profundamente el contenido pienso que de acuerdo a lo hablado anteriormente se puede concluir claramente y con argumentos irrefutables lo que a continuacion se menciona,no sin antes olvidar darle gracias al creador del sitio.', '2019-12-10', '1' ,'3');