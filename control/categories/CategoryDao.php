<?php
require_once(dirname(__FILE__,$levels=2)."/connection/Connection.php");

class CategoryDao{
    
    private $conex;

    public function __construct() {
        $this->conex = Connection::getInstance();
    }
    
    public function insert($descripcion){
        $sql = "INSERT INTO categorias(id, descripcion) VALUES (null,'".$descripcion."');";
        
        try {
            $result = $this->conex->getConnection()->query($sql);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al ingresar los datos");
            return 0;
        }
        
    }
    public function update($id,$descripcion){
        $sql = "UPDATE categorias SET descripcion = '".$descripcion."' WHERE id = ".$id.";";
        try {
            $result = $this->conex->getConnection()->query($sql);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al actualizar los datos");
            return 0;
        }
    }
    public function delete($id){
        $sql = "DELETE FROM categorias WHERE id = ".$id.";";
        try {
            $result = $this->conex->getConnection()->query($sql);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al borrar los datos");
            return 0;
        }
    }
    public function selectAll(){
        $sql = "SELECT * FROM categorias";
        try {
            $select = $this->conex->getConnection()->query($sql);
            $result = mysqli_fetch_all($select);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al consultar los datos");
            return 0;
        }
        
    }
    public function selectById($id){
        $sql = "SELECT * FROM categorias WHERE id = ".$id;
        try {
            $select = $this->conex->getConnection()->query($sql);
            $result = mysqli_fetch_assoc($select);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al consultar los datos");
            return 0;
        }
        
    }
    
}
?>