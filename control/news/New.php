<?php
session_start();
if(!isset($_SESSION['user'])){
    header("location: ../../view/login.php");
}
require('NewDao.php');
require(dirname(__FILE__,$levels=3)."/lib/dompdf/autoload.inc.php");

use Dompdf\Dompdf;

$newDao = new NewDao();

if (!empty($_POST)) {
    $titulo = $_POST["titulo"];
    $detalle = $_POST["detalle"];
    $fecha_sis = date("Y-m-d");
    $img_name = ValidateImage($_FILES["imagen"])["name"];
    $img_path = ValidateImage($_FILES["imagen"])["route"];
    $id_categoria = $_POST["categoria"];

    if ($_POST['opcion']==1) {
        if ($newDao->insert($titulo,$detalle,$fecha_sis,$img_name,$img_path,$id_categoria)) {
            header("location: ../../view/home/0");
        }
    }elseif ($_POST['opcion']==2) {
        $id = $_POST['id'];
        if ($newDao->update($id,$titulo,$detalle,$fecha_sis,$img_name,$img_path,$id_categoria)) {
            header("location: ../../view/home/0");
        }
    }
}


if(!empty($_GET)){
    $id = $_GET['r'];
    
    $dompdf = new Dompdf();
    $dompdf->loadHtml(getHtmlForPdf($id));
    //Setup the paper size and orientation
    $dompdf->setPaper('A4', 'landscape');
    // Render the HTML as PDF
    $dompdf->render();
    // Output the generated PDF to Browser
    $dompdf->stream('noticia'.$id.'.pdf'); 
}

function ValidateImage($image){

    $route="../../view/imgs/".$image['name'];
    $result = array("route"=>"","name"=>"");
    if ($image["size"]<2000000){
        if ($image["type"] =="image/jpeg" || $image["type"] =="image/png"){
            if(move_uploaded_file ($image["tmp_name"], $route)){
                $result["route"] = "imgs/";
                $result["name"] = $image['name'];
            }else{
                $result["route"] = "imgs/";
                $result["name"] = "newsAi1.jpg";
            }    
        }
    }
    return $result;
}


function getHtmlForPdf($id){
    $newDao = new NewDao();
    $news = $newDao->selectForPdf($id);
    
    $html = "<!DOCTYPE html>
            <html lang='en'>
            <head>
                <meta charset='UTF-8'>
                <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                <meta http-equiv='X-UA-Compatible' content='ie=edge'>
                <link rel='icon' href='../../view/imgs/iconTitle.jpg'/>
                <link rel='stylesheet' href='../../view/css/bootstrap.min.css'>
                <link href='https://fonts.googleapis.com/css?family=Roboto&display=swap' rel='stylesheet'>
                <title>Blog</title>
            </head>
            <body style='font-family: 'Roboto', sans-serif;'>
                <header class='bg-dark text-center'>
                    <div class='text-white h2'>
                    <p>Noticiero</p> 
                    </div>
                </header>
                <div class='container'>
                <br>
                <div class='row'>
                    <div class=col-12>";
    
    
    foreach ($news as $noticia) {
        $html.="<div class='card w-100'>
                    <div class='card-header text-center'>".$noticia[1]."</div>
                     
                    <div class='card-body'>
                    <h5 class='card-title text-center'>.</h5>
                    <img src='../../view/".$noticia[5].$noticia[4]."' class='card-img-top img-fluid' alt='noti1'>
                    <p class='card-text'>".$noticia[2]."</p>
                </div>
            </div>
            <br>";    
    }

    $html .="</div>
                </div>
            </div>
            <br>
            <footer class='page-footer font-small blue bg-dark'>
                <!-- Copyright -->
                <div class='footer-copyright text-center text-white py-3'>© 2019 Copyright:
                <a href='#'> RewDev.com</a>
                </div>
                <!-- Copyright -->

                </footer>

            <script src='js/bootstrap.min.js'></script>
        </body>
        </html>";

    return $html;
}
?>