<?php
require_once(dirname(__FILE__,$levels=2)."/connection/Connection.php");

class NewDao{
    
    private $conex;

    public function __construct() {
        $this->conex = Connection::getInstance();
    }
    
    public function insert($titulo,$detalle,$fecha_sis,$img_name,$img_path,$id_categoria){
        $sql = "INSERT INTO noticias (id,titulo,detalle,fecha_sis,img_name,img_path,id_categoria) VALUES (null,'".$titulo."','".$detalle."','".$fecha_sis."','".$img_name."','".$img_path."',".$id_categoria.");";
        
        try {
            $result = $this->conex->getConnection()->query($sql);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al ingresar los datos");
            return 0;
        }
        
    }
    public function update($id,$titulo,$detalle,$fecha_sis,$img_name,$img_path,$id_categoria){
        $sql = "UPDATE noticias SET titulo = ".$titulo.",detalle = '".$detalle."',fecha_sis = '".$fecha_sis."',img_name = '".$img_name."',img_path = '".$img_path."',id_categoria = '".$id_categoria."' WHERE id = ".$id.";";
        try {
            $result = $this->conex->getConnection()->query($sql);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al actualizar los datos");
            return 0;
        }
    }
    public function delete($id){
        $sql = "DELETE FROM noticias WHERE id = ".$id.";";
        try {
            $result = $this->conex->getConnection()->query($sql);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al borrar los datos");
            return 0;
        }
    }
    public function selectAll(){
        $sql = "SELECT * FROM noticias";
        try {
            $select = $this->conex->getConnection()->query($sql);
            $result = mysqli_fetch_all($select);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al consultar los datos");
            return 0;
        }
        
    }
    public function selectCount(){
        $sql = "SELECT * FROM noticias";
        try {
            $select = $this->conex->getConnection()->query($sql);
            $count = mysqli_num_rows($select);
            return $count;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al consultar los datos");
            return 0;
        }
        
    }
    public function selectById($id){
        $sql = "SELECT * FROM noticias WHERE id = ".$id;
        try {
            $select = $this->conex->getConnection()->query($sql);
            $result = mysqli_fetch_assoc($select);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al consultar los datos");
            return 0;
        }
    }
    public function selectForPdf($id){
        $sql = "SELECT * FROM noticias WHERE id = ".$id;
        try {
            $select = $this->conex->getConnection()->query($sql);
            $result = mysqli_fetch_all($select);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al consultar los datos");
            return 0;
        }
    }
    public function selectByIdCategory($id){
        $sql = "SELECT * FROM noticias WHERE id_categoria = ".$id;
        try {
            $select = $this->conex->getConnection()->query($sql);
            $result = mysqli_fetch_all($select);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al consultar los datos");
            return 0;
        }
        
    }
}
?>