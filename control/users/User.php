<?php
require('UserDao.php');
$userDao = new UserDao();

if (!empty($_POST)) {
    if ($_POST['opcion']==1) {
        $nombre = $_POST['nombre'];
        $correo = $_POST['correo'];
        $usuario = $_POST['usuario'];
        $password = $_POST['password'];
        $role = "User";
        if ($userDao->insert($nombre,$correo,$usuario,$password,$role)) {
            header("location: ../../view/login.php");
        }
    }elseif ($_POST['opcion']==2) {
        $id = $_POST['id'];
        $nombre = $_POST['nombre'];
        $correo = $_POST['correo'];
        $usuario = $_POST['usuario'];
        $password = $_POST['password'];
        $role = $_POST['role'];
        if ($userDao->update($id,$nombre,$correo,$usuario,$password,$role)) {
            header("location: ../../view/home/0");
        }
    }elseif ($_POST['opcion']==3) {
        $usuario = $_POST['usuario'];
        $password = $_POST['password'];
        
        if ($userDao->validateLogin($usuario,$password)) {
            $datos = $userDao->selectByUsAndPas($usuario,$password);
            session_start();
            $_SESSION['user']=$usuario;
            $_SESSION['userId']=$datos['id'];
            $_SESSION['role']=$datos['role'];
            $_SESSION['start']=time();
            $_SESSION['expire'] = $_SESSION['start']+(3*60);
            header("location: ../../view/home/0");
        }else{
            print_r("<script>alert('usuario o clave incorrectos!!');
                    window.location='../../view/login.php';
                    </script>");
        }
    }
}
if(!empty($_GET)){
    $id = $_GET['k'];
    if ($userDao->delete($id)) {
        header("location: ../../view/home/0");
    }
}
?>