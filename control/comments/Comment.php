<?php
require('CommentDao.php');
$commentDao = new CommentDao();

if (!empty($_POST)) {
    if ($_POST['opcion']==1) {
        $comentario = $_POST["comentario"];
        $fecha_comment = date("Y-m-d");
        $id_noticia = $_POST["noti"];
        $id_usuario =$_POST["user"];
        if ($commentDao->insert($comentario,$fecha_comment,$id_usuario,$id_noticia)) {
            header("location: ../../view/new/noticia_".$id_noticia);
        }
    }elseif ($_POST['opcion']==2) {
        $id = $_POST['id'];
        $comentario = $_POST["comentario"];
        $fecha_comment = date("Y-m-d");
        $id_noticia = $_POST["noti"];
        $id_usuario =$_POST["user"];
        if ($commentDao->update($id,$comentario,$fecha_comment,$id_usuario,$id_noticia)) {
            header("location: ../../view/home/0");
        }
    }
}
if(!empty($_GET)){
    $id = $_GET['k'];
    if ($commentDao->delete($id)) {
        header("location: ../../view/home/0");
    }
}
?>