<?php
require_once(dirname(__FILE__,$levels=2)."/connection/Connection.php");

class CommentDao{
    
    private $conex;

    public function __construct() {
        $this->conex = Connection::getInstance();
    }
    
    public function insert($comentario,$fecha_comment,$id_usuario,$id_noticia){
        $sql = "INSERT INTO comentarios(id, comentario, fecha_comment, id_usuario, id_noticia) VALUES (null, '".$comentario."', '".$fecha_comment."', ".$id_usuario.", ".$id_noticia.");";
        
        try {
            $result = $this->conex->getConnection()->query($sql);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al ingresar los datos");
            return 0;
        }
        
    }
    public function update($id,$comentario,$fecha_comment,$id_usuario,$id_noticia){
        $sql = "UPDATE comentarios SET comentario = '".$comentario."',fecha_comment = '".$fecha_comment."',id_usuario = ".$id_usuario.",id_noticia = ".$id_noticia." WHERE id = ".$id.";";
        try {
            $result = $this->conex->getConnection()->query($sql);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al actualizar los datos");
            return 0;
        }
    }
    public function delete($id){
        $sql = "DELETE FROM comentarios WHERE id = ".$id.";";
        try {
            $result = $this->conex->getConnection()->query($sql);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al borrar los datos");
            return 0;
        }
    }
    public function selectAll(){
        $sql = "SELECT * FROM comentarios";
        try {
            $select = $this->conex->getConnection()->query($sql);
            $result = mysqli_fetch_all($select);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al consultar los datos");
            return 0;
        }
        
    }
    public function selectById($id){
        $sql = "SELECT * FROM comentarios WHERE id = ".$id;
        try {
            $select = $this->conex->getConnection()->query($sql);
            $result = mysqli_fetch_assoc($select);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al consultar los datos");
            return 0;
        }
        
    }
    
    public function selectByIdNoti($id){
        $sql = "SELECT nombre, correo, comentario, fecha_comment FROM (SELECT usuario.nombre, usuario.correo, comentarios.comentario, comentarios.fecha_comment, comentarios.id_noticia FROM comentarios JOIN usuario where comentarios.id_usuario = usuario.id)as perrito WHERE id_noticia = ".$id;
        try {
            $select = $this->conex->getConnection()->query($sql);
            $result = mysqli_fetch_all($select);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al consultar los datos");
            return 0;
        }
    }
    public function selectCant($id){
        $sql = "select count(id) as coment from comentarios where id_noticia= ".$id;
        try {
            $select = $this->conex->getConnection()->query($sql);
            $result = mysqli_fetch_assoc($select);
            return $result;
        } catch (Exception $e) {
            print_r("Oops Ha ocurrido un error al consultar los datos");
            return 0;
        }
    }
}
?>