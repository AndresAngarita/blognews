<?php
require_once (dirname(__FILE__,$levels=2).'/Config.php');
class Connection{
    
    //Contenedor de la instancia de nuestra clase
    private static $instance = NULL;
    private $con = NULL;

    //El constructor es privado para prevenir la creacion de objetos via new
    private function __construct() {
        $this->con = mysqli_connect(Config::host,Config::user,Config::pass,Config::base);
    }
    //clone no permitido
    private function __clone(){}
    
    //metodo singleton : sirve para traer una instancia de esta misma clase 
    public static function getInstance(){
        if (is_null(self::$instance)) {
            self::$instance = new Connection();
        }
        
        return self::$instance;
    }
    
    //metodo que retorna la conexion
    public function getConnection(){
        return $this->con;
    }
}
?>